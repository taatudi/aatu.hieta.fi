from flask import Flask, request, session, g, redirect, url_for, abort,\
    render_template, flash, Markup
import sqlite3
from markdown import markdown
import os

app = Flask(__name__)
app.config.update(dict(
    DATABASE = os.path.join(app.root_path, 'sqlite3.db'),
    DEBUG = True,
    SECRET_KEY = 'WatWat.'
))

def connect_db():
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db

@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

def db_init():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def get_nav():
    db = get_db()

    links = db.execute('SELECT pages.title, links.link, links.icon FROM links, pages WHERE pages.id = links.page_id').fetchall()
    return links

@app.route('/')
def hello():
    return show_page(1)

@app.route('/page/<link>')
def show_page(link):
    db = get_db()
    id = 0
    if not type(link) == int:
        id = db.execute('SELECT page_id FROM links WHERE link=?',(link,)).fetchone()[0]
    else:
        id = link
    cur = db.execute('SELECT id, title, content FROM pages WHERE id=?', (id,))

    r = cur.fetchone()
    if r == None:
        page = dict(nav=get_nav())
        return render_template('db-error.html', page=page)

    page = dict(title=r[1],
        content = Markup(markdown(r[2])),
        nav = get_nav())
    return render_template('page.html', page=page)

if __name__ == '__main__':
    app.run(host='0.0.0.0')